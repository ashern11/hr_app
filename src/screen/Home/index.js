 import React from 'react';
 import { StyleSheet, Image } from 'react-native';
 import {
   ApplicationProvider,
   Button,
   Icon,
   IconRegistry,
   Layout,
   Text,
 } from '@ui-kitten/components';
 import { EvaIconsPack } from '@ui-kitten/eva-icons';
 import * as eva from '@eva-design/eva';
 import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSelector, useDispatch } from 'react-redux';
 
 
 const  App = ({navigation}) => {
  const {employee_picture} = useSelector(state => state.LoginReducer);
  console.log(employee_picture);

  // Auth
  const dispatch = useDispatch();
  const onLogout = async () => {
    try {
      await AsyncStorage.removeItem('employee');
      dispatch({type: "LOGOUT_SUCCESS"});
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  }
  // End Auth

  return(
   <>
     <IconRegistry icons={EvaIconsPack}/>
     <ApplicationProvider {...eva} theme={eva.light}>
       <Layout style={styles.container}>

        {employee_picture == null ? 
          <Image
              style={styles.avatar}
              source={ require('../../asset/images/as.png') }
            />
          :
          <Image
              source={{
                  uri: `data:image/jpeg;base64,${employee_picture}`
              }}
              style={styles.avatar}
          />
        }
 
         <Button style={styles.likeButton} status="danger"  onPress={ () => navigation.navigate('Biodata')}>
           Lengkapi Biodata
         </Button>

         <Button style={styles.likeButton} status="success">
           Request Verifikasi Data
         </Button>

         <Button style={styles.btnLogout} status="primary" onPress={ () => onLogout() }>
           Logout
         </Button>
         
       </Layout>
     </ApplicationProvider>
   </>
  )
 };
 
 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   text: {
     textAlign: 'center',
   },
   likeButton: {
     marginVertical: 16,
     marginHorizontal: 10,
     alignSelf: 'stretch',
   },
   btnLogout: {
    marginVertical: 16,
    borderRadius: 50,
  },
   avatar:{
     borderRadius: 100,
     width: 200,
     height: 200
   }
 });
 
 export default App;