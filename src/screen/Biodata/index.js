import React, { useState, useEffect } from 'react';
import { StyleSheet, View, ActivityIndicator, ToastAndroid } from 'react-native';
import {
  ApplicationProvider,
  Button,
  Icon,
  IconRegistry,
  Layout,
  Text,
  Input,
  IndexPath,
  Select,
  SelectItem,
  Datepicker,
  Spinner,
} from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';
import { ScrollView } from 'react-native-gesture-handler';
import { FieldFormOne } from '../../config/BiodataFormFirst';
import { API_URL } from '../../config';
import axios from 'axios'
import { useSelector } from 'react-redux';


const  App = ({navigation}) => {

    const {employee} = useSelector(state => state.LoginReducer);

    const [form , setForm] = useState({});
    const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0));
    const [loading, setLoading] = useState(true);

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input] : value,
        })
    }

    useEffect( () => {
        getData()
    }, [])

    const getData = async() => {
        axios.get(API_URL + '/employee/get/' + employee, {})
            .then(res => {

                console.log('Get Data Employee', res.data)
                setForm(res.data.data)
                setLoading(false);

            }).catch(err => {
                console.log('err', err.response );
            });
    }

    const sendData = async() => {
        console.log("Data :", form);
        
        const payload = new FormData();
        payload.append('idemployee', employee);
        payload.append('status', 10);
        FieldFormOne.map((item, index) => {
            if(item.type != 'select' && item.type != 'date' ){
                if(form[item.name] !== undefined){
                    payload.append(item.name.toLowerCase(), form[item.name]);
                }
            }
        })

        console.log(payload);

        axios
            .post( API_URL + '/employee/save', payload)
            .then(res => {

            // If Success
            let response = res.data
            console.log('success', response )

            ToastAndroid.show(response.message, ToastAndroid.SHORT);
            navigation.goBack();
            
            }).catch(err => {
            console.log('err', err );
            });
    }


 const renderForm = () => {
    return FieldFormOne.map((item, index) =>
        { 

            if(item.type == 'select'){
                return <Select
                            key={index}
                            label={item.label}
                            style={styles.formInput}
                            selectedIndex={form[item.name]}
                            onSelect={index => onInputChange(index, item.name)}>
                                {item.value.map((option, index) => 
                                    <SelectItem key={index} title={option.title}/>
                                )}
                        </Select>

            }else if(item.type == 'date'){

                return  <Datepicker
                            key={index}
                            label={item.label}
                            style={styles.formInput}
                            date={form[item.name]}
                            onSelect={ (value) => onInputChange(value, item.name) }
                        />
            }else{   

                return <Input
                            key={index}
                            label={item.label}
                            value={form[item.name]}
                            placeholder={item.label}
                            style={styles.formInput}
                            onChangeText={ (value) => onInputChange(value, item.name) }
                        />
            }
    })
  }


   // return a Spinner when loading is true

 return(

  <>
    <IconRegistry icons={EvaIconsPack}/>
    <ApplicationProvider {...eva} theme={eva.light}>
      {loading ? <View
          style={{
            ...StyleSheet.absoluteFill,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Spinner size='giant'/>
      </View> :

      <ScrollView>
          
        <Text style={styles.text} category='h6'>
            I. DATA PRIBADI / BIODATA
        </Text>

        {renderForm()}

        <Button style={styles.likeButton}  onPress={ () => sendData() }>
            Simpan
        </Button>

      </ScrollView>
      }
    </ApplicationProvider>
  </>
 )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    marginBottom: 20,
  },
  likeButton: {
    marginHorizontal: 10,
    marginVertical: 16,
  },
  avatar:{
    borderRadius: 100,
    width: 200,
    height: 200
  },
  formInput:{
    marginHorizontal: 10,
    marginVertical: 5,
  }
});

export default App;