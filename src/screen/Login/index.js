import React, { useState } from 'react';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import * as eva from '@eva-design/eva';
import {
  ApplicationProvider,
  Button,
  Icon,
  IconRegistry,
  Layout,
  Text,
  Input,
} from '@ui-kitten/components';
import {
  StyleSheet,
  ToastAndroid,
  Image,
} from 'react-native';
import { API_URL } from '../../config';

import axios from 'axios'
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useSelector, useDispatch} from 'react-redux';

 
 const Login  = () => {

    const LoginReducer = useSelector(state => state.LoginReducer);
    const dispatch = useDispatch();

    const [form , setForm] = useState({
      username: '',
      password: ''
    })

    const onInputChange = (value, input) => {
      setForm({
        ...form,
        [input] : value,
      })
    }

    const sendData = async() => {
      console.log("Data :", form);

      const payload = new FormData();
      payload.append('username', form.username);
      payload.append('password', form.password);

      axios
        .post( API_URL + '/auth/login', payload,
          {
            headers: {
              "Accept": "application/json",
              "Content-type": "application/json",
              "Authorization": "Bearer SFhyZUk3UkkyaWxLOVI2WA==",
            },
        })
        .then(res => {
          let response = res.data

          console.log('success', response )
          let message  = response.message[0];

          ToastAndroid.show(message, ToastAndroid.SHORT);

          if(response.status == true){
            AsyncStorage.setItem('employee', response.data.idEmployee)
            AsyncStorage.setItem('employee_picture', response.data.Picture)
            
            dispatch({type: 'LOGIN_SUCCESS', employee: response.data.idEmployee, employee_picture: response.data.Picture})
            console.log(response.data.idEmployee)
          }
          
        }).catch(err => {
          console.log('err', err );
          // ToastAndroid.show(err.response.message, ToastAndroid.SHORT);
        });
    }


    return(
    <>
      <IconRegistry icons={EvaIconsPack}/>
      <ApplicationProvider {...eva} theme={eva.light}>
        <Layout style={styles.container}>

        <Image 
            style={styles.logo}
            source={ require('../../asset/images/anper.png') } />

          <Input
              placeholder='NIK'
              style={styles.formInput}
              onChangeText={ (value) => onInputChange(value, 'username') }
            />

          <Input
              placeholder='Password'
              style={styles.formInput}
              onChangeText={nextValue => setValue(nextValue)}
              onChangeText={ (value) => onInputChange(value, 'password') }
            />
        
          <Button style={styles.likeButton}  onPress={ () => sendData() }>
            Login
          </Button>
        </Layout>
      </ApplicationProvider>
    </>
    )
 };
 
 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   text: {
     textAlign: 'center',
   },
   likeButton: {
     marginVertical: 16,
     borderColor: 'red',
     backgroundColor: 'red',
   },
   logo: {
    marginBottom: 40,
   },
   formInput:{
     marginHorizontal: 10,
     marginVertical: 10,
   }
 });
 
 export default Login;