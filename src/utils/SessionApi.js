

import axios from 'axios';

class SessionApi {
  static apiUrl =  window.location.host === false ? "http://localhost:8000" : "http://woodepic.xyz";

  static headers = {
    "Accept": "application/json",
    "Content-type": "application/json",
  }
  static authencticationHeader = {
    "Accept": "application/json",
    "Authorization": "Bearer " + localStorage.jwt,
    "Content-type": "application/json",
  }

  static login(credentials) {
    
    var payload = {
      "grant_type": "password",
      "client_id": "2",
      "client_secret": "9kvcNAM7Mpba2hlhdAkaWWHcPlNXiS1feoPpy2QX",
      "username": credentials.email,
      "password": credentials.password
    }
    return axios.post(this.apiUrl + '/oauth/token', payload)
      .then(res => {
        return res.data
      }).catch(err => {

        throw err;
      });

  }

  static post(url, payload) {

    return axios.post(this.apiUrl + url, payload, {
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + localStorage.jwt,
        "Content-type": "application/json",
      },
      params: {
        "timestamp": new Date().getTime()
      }
    })
      .then(res => {
        return res.data
      }).catch(err => {

        throw err;
      });

  }

  static get(url, queryParams = {}) {

    queryParams = queryParams
    queryParams['timestamps'] = new Date().getTime();
    console.log(localStorage.jwt);

    return axios.get(this.apiUrl + url, {
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + localStorage.jwt,
        "Content-type": "application/json",
      },
      params: queryParams
    })
      .then(res => {
        return res.data
      }).catch(err => {

        throw err;
      });

  }
  static getWithoutStatus(url) {
    console.log(localStorage.jwt);

    return axios.get(this.apiUrl + url, {
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + localStorage.jwt,
        "Content-type": "application/json",
      },
      params: {
        "timestamp": new Date().getTime()
      }
    })
      .then(res => {
        return res.data.data
      }).catch(err => {

        throw err;
      });

  }
  
  static delete(url) {
    console.log(localStorage.jwt);

    return axios.delete(this.apiUrl + url, {
      headers: {
        "Accept": "application/json",
        "Authorization": "Bearer " + localStorage.jwt,
        "Content-type": "application/json",
      },
      params: {
        "timestamp": new Date().getTime()
      }
    })
      .then(res => {
        return res.data
      }).catch(err => {

        throw err;
      });

  }


  static getProfile() {
    if(this.isProduction) {
      this.apiUrl =  "http://distriboost.gamma.co.id";

    }


    if (localStorage.getItem("jwt") != null || localStorage.getItem("jwt") != undefined) {

      return SessionApi.get("/api/v1/user/detail")
    }
  }

}


export default SessionApi;
