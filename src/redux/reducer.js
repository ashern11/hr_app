import { combineReducers } from 'redux';

const initialState = {
    name: 'Development'
};

const globalReducer = (state = initialState, action) => {
    return state
}

const initialStateScreen = {
    idScreen: null
}

const ScreenReducer = (state = initialStateScreen, action) => {
  switch (action.type) {
    case "CHANGE_ID":
      return {
        idScreen: action.idScreen
      }
    default:
      return state
  }
}

const initialStateLogin = {
    isLogin: false,
    employee: null,
    employee_picture: null,
}

const LoginReducer = (state = initialStateLogin, action) => {
  switch(action.type) {
    case "LOGIN_SUCCESS":
      return {
        ...state,
        isLogin: true,
        employee: action.employee,
        employee_picture: action.employee_picture
      }
    case "LOGOUT_SUCCESS":
        return {
        ...state,
        isLogin: false,
        employee: null,
        employee_picture: null
      }
    default: 
      return state;
  }
}


const reducer = combineReducers({
    globalReducer,
    ScreenReducer,
    LoginReducer
})


export default reducer;