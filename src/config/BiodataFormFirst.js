
export const FieldFormOne = [
    {
        name: 'NamaLengkap',
        label: 'Nama Lengkap/Full Name'
    },
    {
        name: 'Alamat',
        label: 'Alamat/Address'
    },
    {
        name: 'KotaLahir',
        label: 'Tempat Lahir'
    },
    {
        name: 'tanggallahir',
        label: 'Tanggal Lahir',
        type: 'date'
    },
    {
        name: 'kewarganegaraan',
        label: 'Kewarganegaraan',
        type: 'select',
        value: [
            {
                title: 'WNI',
                value:  'WNI'
            },
            {
                title: 'WNA',
                value:  'WNA'
            }
        ]
    },
    {
        name: 'NoKTP',
        label: 'No.KTP / Identity Card No.'
    },
    {
        name: 'NoNPWP',
        label: 'No. NPWP'
    },
    {
        name: 'agama',
        label: 'Agama',
        type: 'select',
        value: [
            {
                title: 'Islam',
                value:  'Islam'
            },
            {
                title: 'Kristen',
                value:  'Kristen'
            },
            {
                title: 'Katolik',
                value:  'Katolik'
            },
            {
                title: 'Hindu',
                value: 'Hindu'
            },
            {
                title: 'Budha',
                value: 'Budha'
            },
            {
                title: 'Konghucu',
                value: 'Konghucu'
            }
        ]
    },
    {
        name: 'statuskawin',
        label: 'Status Perkawinan',
        type: 'select',
        value: [
            {
                title: 'Kawin',
                value:  'Islam'
            },
            {
                title: 'Belum Kawin',
                value:  'Kristen'
            },
        ]
    },
    {
        name: 'goldar',
        label: 'Golongan Darah / Blood Type',
        type: 'select',
        value: [
            {
                title: 'A',
                value:  'A'
            },
            {
                title: 'B',
                value:  'B'
            },
            {
                title: 'AB',
                value:  'AB'
            },
            {
                title: 'O',
                value: 'O'
            }
        ]
    },
    {
        name: 'kegemaran',
        label: 'Kegemaran'
    },
 ];