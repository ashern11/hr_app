export const FieldFormSconde = [
    {
        name: 'NamaKursus',
        label: 'Nama Kursus / Course Name'
    },
    {
        name: 'Penyelenggara',
        label: 'Penyelenggara / Organizer'
    },
    {
        name: 'Tempat',
        label: 'Tempat / Waktu / Lainnya'
    },
    {
        name: 'Ijazah',
        label: 'Berijazah / Tidak Berijazah'
    },
];

export const FieldFormFour = [
    {
        name: 'Bahasa',
        label: 'Bahasa / Language'
    },
    {
        name: 'Membaca',
        label: 'Membaca / Reading'
    },
    {
        name: 'Menulis',
        label: 'Menulis / Writing'
    },
    {
        name: 'Bicara',
        label: 'Bicara / Communicating'
    },
];

export const FieldFormFive = [
    {
        name: 'Nama',
        label: 'Nama & Alamat Perusahaan'
    },
    {
        name: 'Jenis Usaha',
        label: 'Jabatan'
    },
    {
        name: 'Jabatan',
        label: 'Jabatan'
    },
    {
        name: 'Dari',
        label: 'Dari'
    },
    {
        name: 'Sampai',
        label: 'Sampai'
    },
]

export const FieldFormSix = [
    {
        name: 'uraian',
        label: 'Uraian Tugas & Tanggung Jawab'
    },
];

export const FieldFormStrukturOrganisasi = [
    {
        name: 'Gambaran',
        label: 'Gambaran Struktur'
    },
];

export const FieldFormEight = [
    {
        name: 'Tinggal',
        label: 'Anda tinggal dirumah',
        type: 'select',
        value: [
            {
                name: 'Sendiri',
                value: 'Sendiri'
            },
            {
                name: 'IstriAnak',
                label: 'Bersama Istri & Anak'
            },
            {
                name: 'Bersama Orang Tua',
                label: 'Besama Orang Tua'
            },
            {
                name: 'Kaka / Adik / Keluarga dekat',
                label: 'Kaka / Adik / Keluarga dekat'
            },
            {
                name: 'Teman / Lain-lain',
                label: 'Teman / Lain-lain'
            },
        ]
    },
    {
        name: 'StatusRumah',
        label: 'Status Rumah Anda',
        type: 'select',
        value: [
            {
                name: 'Rumah Pribadi',
                label: 'Rumah Pribadi'
            },
            {
                name: 'Rumah Keluarga',
                label: 'Rumah Keluarga'
            },
            {
                name: 'Kontrak / Sewa',
                label: 'Kontrak / Sewa'
            },
            {
                name: 'Kost',
                label: 'Kost'
            },
            {
                name: 'Lainlain',
                label: 'Lain-lain'
            },
        ]
    },
    {
        name: 'Nama',
        label: 'Nama'
    },
    {
        name: 'HubKeluarga',
        label: 'Hubungan Keluarga'
    },
    {
        name: 'Usia',
        label: 'Usia'
    },
    {
        name: 'Pekerjaan',
        label: 'Pekerjaan'
    },
    {
        name: 'Alamat',
        label: 'Alamat'
    },
]

export const FieldFormInformasiLain = [
    {
        name: 'Nama',
        label: 'Nama / Name'
    },
    {
        name: 'Alamat',
        label: 'Alamat & No. Telp / Address & No Phone'
    },
    {
        name: 'Pekerjaan',
        label: 'Pekerjaan / Job'
    },
]

export const FieldFormSignature = [
    {
        name: 'Signature',
        label: 'Tanda Tangan'
    },
];