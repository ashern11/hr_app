import React, {Component, useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';


import { Provider } from 'react-redux'
import {store} from './src/redux/';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, createSwitchNavigator } from '@react-navigation/stack';
import Login from './src/screen/Login';
import Home from './src/screen/Home';
import Biodata from './src/screen/Biodata';
import { connect, useSelector, useDispatch } from 'react-redux';


const Stack = createStackNavigator();

const Router = () => {
  const {isLogin} = useSelector(state => state.LoginReducer);
  const dispatch = useDispatch();

  async function checkLogin () {
    // const token = await AsyncStorage.getItem("token")
    // console.log(token) 
    
    const employee = await AsyncStorage.getItem("employee")
    const employee_picture = await AsyncStorage.setItem('employee_picture', response.data.Picture)
    console.log('employee: ', employee) 
    console.log('isLogin: ', isLogin) 

    if (employee === undefined || employee === null) {
      return dispatch({type: 'LOGOUT_SUCCESS'})
    } else {
      return dispatch({type: 'LOGIN_SUCCESS', employee: employee, employee_picture: employee_picture})
    }
  }

  useEffect( () => {
    checkLogin();
  }, [isLogin])

  return(
    <NavigationContainer>
          <Stack.Navigator>
          {isLogin === true ? (
              <>
                <Stack.Screen name="Home" component={Home}  options={{ headerShown: false }} />
                <Stack.Screen name="Biodata" component={Biodata} />
              </>
              ) : (
              <>
                <Stack.Screen name="Login" component={Login} />
              </>
          )}
          </Stack.Navigator>
    </NavigationContainer>
  );
}


class App extends Component {

  render(){
    return(
      <Provider store={store}>
        <Router />
      </Provider>
    )
  };
}

export default App;